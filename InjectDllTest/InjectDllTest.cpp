// InjectDllTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <shlwapi.h>
#include <conio.h>

#pragma comment(lib, "shlwapi.lib")


void usage()
{
	printf("loadInjectDll.exe -dll=<Inject DLL> [-pid=<target pid> | -exe=<full path of exe>]\n");
}

int _tmain(int argc, _TCHAR* argv[])
{
	int ret = NO_ERROR;
	TCHAR* dll_name = NULL;
	TCHAR* target_pid = NULL;
	TCHAR* target_exe = NULL;

	for (int i = 0; i<argc; i++)
	{
		if (argv[i] != NULL && ((argv[i][0] == L'-') || (argv[i][0] == L'/')))
		{
			if (_tcsncicmp(&argv[i][1], _T("debug"), 5) == 0)
			{
				printf("Press any key to continue...\n");
				_getch();
			}
			else if (_tcsncicmp(&argv[i][1], _T("dll"), 3) == 0)
			{
				TCHAR* p = _tcschr(&argv[i][1], L'=');
				if (p != NULL)
				{
					p++;
					dll_name = p;
				}
			}
			else if (_tcsncicmp(&argv[i][1], _T("pid"), 3) == 0)
			{
				TCHAR* p = _tcschr(&argv[i][1], L'=');
				if (p != NULL)
				{
					p++;
					target_pid = p;
				}
			}
			else if (_tcsncicmp(&argv[i][1], _T("exe"), 3) == 0)
			{
				TCHAR* p = _tcschr(&argv[i][1], L'=');
				if (p != NULL)
				{
					p++;
					target_exe = p;
				}
			}
		}
	}

	if (dll_name != NULL)
	{
		ret = NO_ERROR;
		if (!PathFileExists(dll_name))
		{
			printf("Library %s not found\n", dll_name);
			return ERROR_FILE_NOT_FOUND;
		}
		int pid = 0;
		if (target_pid != NULL)
		{
			pid = _tstoi(target_pid);

			if (pid>0)
			{
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
				if (hProcess != NULL)
				{
					printf("OpenProcess Successfully.\n");
					LPVOID vAddress = VirtualAllocEx(hProcess, NULL, MAX_PATH, MEM_COMMIT, PAGE_READWRITE);
					if (vAddress)
					{
						printf("VirtualAllocEx Successfully.\n");
						if (WriteProcessMemory(hProcess, vAddress, dll_name, (_tcslen(dll_name) + 1)*sizeof(TCHAR), NULL))
						{
							printf("WriteProcessMemory Successfully.\n");
							HMODULE hKernel32 = ::GetModuleHandle(_T("Kernel32"));
							if (hKernel32 != NULL)
							{
								HANDLE hThread;
								if (sizeof(TCHAR) == 1)
									hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(hKernel32, "LoadLibraryA"), vAddress, 0, NULL);
								else
									hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(hKernel32, "LoadLibraryW"), vAddress, 0, NULL);
								if (hThread != NULL)
								{
									printf("CreateRemoteThread Successfully.\n");
									while (::ResumeThread(hThread)>1)NULL;
									WaitForSingleObject(hThread, INFINITE);
								}
								else
								{
									ret = GetLastError();
									printf("create remote thread fail, %d\n", ret);
								}
							}
							else
							{
								ret = GetLastError();
								printf("get kernel32 module handle fail, %d\n", ret);
							}
						}
						else
						{
							ret = GetLastError();
							printf("write virtual addr fail, %d\n", ret);
						}
						VirtualFreeEx(hProcess, vAddress, MAX_PATH, MEM_RELEASE);
					}
					else
					{
						ret = GetLastError();
						printf("open virtual addr fail, %d\n", ret);
					}
					CloseHandle(hProcess);
				}
				else
				{
					ret = GetLastError();
					printf("open target pid wrong, %d\n", pid);
				}
			}
			else
			{
				printf("target pid wrong, %d\n", pid);
				ret = ERROR_INVALID_PARAMETER;
			}
		}
		else
		{
			// create process by target_exe
			if (PathFileExists(target_exe))
			{
				PROCESS_INFORMATION pi;
				STARTUPINFO si;
				ZeroMemory(&si, sizeof(STARTUPINFO));
				si.cb = sizeof(STARTUPINFO);
				si.dwFlags = STARTF_USESHOWWINDOW;
				si.wShowWindow = SW_SHOW;
				//if (CreateProcess(NULL, target_exe, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL,NULL,&si,&pi))
				if (CreateProcess(NULL, target_exe, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
				{
					if (FALSE)
					{
						HANDLE hProcess = pi.hProcess;
						LPVOID vAddress = VirtualAllocEx(hProcess, NULL, MAX_PATH, MEM_COMMIT, PAGE_READWRITE);
						if (vAddress)
						{
							if (WriteProcessMemory(hProcess, vAddress, dll_name, (_tcslen(dll_name) + 1)*sizeof(TCHAR), NULL))
							{
								HMODULE hKernel32 = ::GetModuleHandle(_T("Kernel32"));
								HANDLE hThread;
								if (sizeof(TCHAR) == 1)
									hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(hKernel32, "LoadLibraryA"), vAddress, 0, NULL);
								else
									hThread = CreateRemoteThread(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(hKernel32, "LoadLibraryW"), vAddress, 0, NULL);
								if (hThread != NULL)
								{
									while (::ResumeThread(hThread)>1)NULL;
									WaitForSingleObject(hThread, INFINITE);
								}
							}
							VirtualFreeEx(hProcess, vAddress, MAX_PATH, MEM_RELEASE);
						}
					}
					//while (::ResumeThread(pi.hThread)>1)NULL;
					//WaitForSingleObject(pi.hProcess,INFINITE);
					CloseHandle(pi.hProcess);
					CloseHandle(pi.hThread);
					pid = pi.dwProcessId;
				}
				else
				{
					ret = GetLastError();
					printf("create process fail, %d\n", ret);
				}
			}
			else
			{
				ret = GetLastError();
				printf("Target %s not found\n", target_exe);
			}
		}
	}
	else
	{
		usage();
		ret = ERROR_INVALID_PARAMETER;
	}

	return 0;

}

