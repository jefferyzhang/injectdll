// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <atlstr.h>


#define SAFE_DELETE(p) do{ if(NULL!=p) {delete p; p = NULL;}} while(0)
#define SAFE_FREE(p) do{ if(NULL!=p) {free(p); p = NULL;}} while(0)


// TODO: reference additional headers your program requires here
// from util.cpp
void set_path();
DWORD SetFunctionAddressV2(HMODULE hModule, LPCSTR szDllName, short ordinal, DWORD* lpOriginal, DWORD lpNewAddress);
DWORD SetFunctionAddress(HMODULE hModule, LPCSTR szDllName, LPCSTR szFuncName, DWORD* lpOriginal, DWORD lpNewAddress);
void SetJump(BOOL set, DWORD lpAddress, LPBYTE buffer, DWORD lpAddressJump);

// from logit.cpp
void logIt(TCHAR* fmt, ...);